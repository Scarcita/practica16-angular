import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padreMensaje1 = ' Hijo 1 ';
  padreMensaje2 = ' Hijo 2 ';
  padreMensaje3 = ' Hijo 3 ';
  padreMensaje4 = ' Hijo 4 ';
  padreMensaje5 = ' Hijo 5 ';
  
  
  constructor() { }

  ngOnInit(): void {
  }

}
